﻿1. If you don't have Visual Studio installed, please download and install the free Community Edition 
here. This exercise also includes a Gulp configuration files that you can use with other editors. 
Read the readme.txt file for instructions on how to use this (the instructions assume you are familiar 
with Node.js and Gulp).

2. Next, download the Module 5 Exercise project and unzip it to a folder on your computer.

3. Open the "Cookbook-Module5.csproj" file from the Cookbook folder Visual Studio. In Visual Studio, 
right-click on cookbook.html and select Set as Start Page before you begin.

4. First you'll update the FoodGroup class in the foodGroup.ts file. Complete the TODO item in the file. You'll be creating a set and get block for the private member variable.

5. Next you'll update the BaseRecipeCategory class in the baseRecipeCategory.ts file. Look for the TODO 
items update the code. You'll be creating set and get blocks for both of the private member variables 
in the file. Save your work.

6. In this exercise, we've also added a RecipeLoader class with functions in the recipeLoader.ts. 
Please review the code in this file to get a sense of the project's architecture. The mapdata function 
loads the data from the JSON file and then loops through the items in the collection and maps them to 
members in the instantiated classes. You can step through the code in your editor and look at the 
variables as they are filled to better understand how this works.

7. When you've completed your work, compile the app (if you're using a watcher, the files should be 
compiled when you save them) and open cookbook.html in your browser to see the site.