﻿1. Open the "WorkingWithFunctions.sln" file in Visual Studio. There are three separate examples in 
this project and you can explore each by right-clicking on each of the .html files in the project 
and choosing "View in Browser" in Visual Studio. For this exercise, we'll be modifying the 
"Optional, Default, Rest Parameters" sample which is a simple home address creator.

2. First, we'll update the user interface. Double-click on `optionalDefaultRestParameters.html` to 
open it in the editor.

3. Let's add a new field to capture the state for the address the user enters. Below this code:
```html
<div class="row">
	<div class="col-md-2">City:</div>
	<div class="col-md-8"><input type="text" id="city" class="form-control" value="Seattle" />
</div>
```
Add the following code:
```html
<div class="row">
	<div class="col-md-2">State:</div>
	<div class="col-md-8"><input type="text" id="state" class="form-control" value="Washington" />
</div>
```

6. Save your work and set `optionalDefaultRestParameters.html` as the start page by right-clicking on 
that file and choosing "Set as Start Page" from the context menu. Now run the project. You should 
see a new "State" field on the form with "Washington" pre-filled (if you don't see the new field, 
refresh the page.

7. Now press the Save button. You'll notice that the State field information is not included. This is 
because we haven't updated the TypeScript code to include that information. Here's your assignment:

8. Modify the TypeScript code to include the new "state" parameter. You'll do this both for the 
"Optional parameter" code and the "Rest parameter" code in optionalDefaultRestParameters.ts. 
Note: you'll need to comment and uncomment the event handlers at the bottom of the functions in 
order to test the two scenarios.